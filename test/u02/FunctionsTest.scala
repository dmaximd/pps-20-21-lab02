package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class FunctionsTest {

  val evenNumber = 4
  val oddNumber = 5
  val empty: String => Boolean = _==""
  val x = 3
  val y = 6
  val z = 9

  val parity: Int => String = {
    case v if v%2 == 0 => "even"
    case _ => "odd"
  }

  val neg: (String => Boolean) => (String => Boolean) = (p => (v => !p(v)))

  val greater: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z

  val greaterCurr: Int => Int => Int => Boolean = x => y => z => (x <= y && y <= z)

  def parityMet(v: Int): String = v match {
    case v if v%2 == 0 => "even"
    case _ => "odd"
  }

  def negMet[A](p: A => Boolean): (A => Boolean) = {
    !p(_)
  }

  def greaterMet(x: Int, y: Int, z: Int): Boolean = (x, y, z) match {
    case (x, y, z) if (x <= y && y <= z) => true
    case _ => false
  }

  def greaterCurrMet(x: Int)(y: Int)(z: Int): Boolean = (x, y, z) match {
    case (x, y, z) if (x <= y && y <= z) => true
    case _ => false
  }

  def compose(f: Int => Int, g: Int => Int): Int => Int = x => f(g(x))

  def composeGeneric[A, B, C](f: B => C, g: A => B): A => C = x => f(g(x))

  def fib(n: Int): Int = n match{
    case 0 => 0
    case 1 => 1
    case _ => fib(n-1) + fib(n-2)
  }

  @Test def testParity(): Unit ={
    assertEquals("even", parity(evenNumber))
    assertEquals("odd", parity(oddNumber))
  }

  @Test def testParityMethod(): Unit ={
    assertEquals("even", parityMet(evenNumber))
    assertEquals("odd", parityMet(oddNumber))
  }

  @Test def testNegLambda(): Unit ={
    val notEmpty = neg(empty)
    testNeg(notEmpty)
  }

  @Test def testNegMethod(): Unit ={
    val notEmpty = negMet(empty)
    testNeg(notEmpty)
  }

  @Test def testGeneric(): Unit ={
    val even: Int => Boolean = num => num % 2 == 0;
    val odd = negMet(even)
    assertTrue(odd(oddNumber))
    assertFalse(odd(evenNumber))
  }

  @Test def testCurryingLambda(): Unit ={
    assertTrue(greater(x, y, z))
    assertTrue(greaterCurr(x)(y)(z))
    assertFalse(greater(y, z, x))
    assertFalse(greaterCurr(y)(z)(x))
  }

  @Test def testCurryingMethod(): Unit ={
    assertTrue(greaterMet(x, y, z))
    assertTrue(greaterCurrMet(x)(y)(z))
    assertFalse(greaterMet(y, z, x))
    assertFalse(greaterCurrMet(y)(z)(x))
  }

  @Test def testCompose(): Unit ={
    assertEquals(9, compose(_-1, _*2)(5))
  }

  @Test def testComposeGeneric(): Unit ={
    val diff: Int => Int = v => v-1
    val mul: Int => Int = v => v*2
    val mod: Int => String = n => n%2 match {
      case 0 => "even"
      case _ => "odd"
    }
    val isEven: String => Boolean = {
      case "even" => true
      case _ => false
    }
    assertEquals(9, composeGeneric(diff, mul)(5))
    assertTrue(composeGeneric(isEven, mod)(4))
    assertFalse(composeGeneric(isEven, mod)(5))
  }

  @Test def testFibonacci(): Unit ={
    assertEquals(0, fib(0))
    assertEquals(1, fib(1))
    assertEquals(1, fib(2))
    assertEquals(2, fib(3))
    assertEquals(3, fib(4))
  }

  def testNeg(p: String => Boolean): Unit = {
    assertTrue(p("foo"))
    assertFalse(p(""))
    assertTrue(p("foo") && !p(""))
  }
}
