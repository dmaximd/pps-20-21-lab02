package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Shape._

class ShapeTest {

  val rectangle: Rectangle = Rectangle(10, 5)
  val square: Square = Square(5)
  val circle: Circle = Circle(5)
  val circlePer: Double = Math.PI*5*2
  val circleArea: Double = Math.PI*5*5

  @Test def testPerimeter(): Unit ={
    assertEquals(30, perimeter(rectangle))
    assertEquals(20, perimeter(square))
    assertEquals(circlePer, perimeter(circle))
  }

  @Test def testArea(): Unit ={
    assertEquals(50, area(rectangle))
    assertEquals(25, area(square))
    assertEquals(circleArea, area(circle))
  }
}
