package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Optionals.Option._

class OptionTest {

  val element: Some[Int] = Some(5)
  val empty: None[Int] = None()
  val predicate: Int => Boolean = _ > 2

  @Test def testFilter(): Unit ={
    assertEquals(element, filter(element)(predicate))
    assertEquals(empty, filter(element)(_>8))
  }

  @Test def testMap(): Unit ={
    assertEquals(Some(true), map(element)(predicate))
    assertEquals(empty, map(empty)(predicate))
  }

  @Test def testCompose(): Unit ={
    assertEquals(Some(true), map2(element, Some(7))((a, b) => a < b))
    assertEquals(Some(false), map2(element, Some(1))((a, b) => predicate(a) && predicate(b)))
  }
}
