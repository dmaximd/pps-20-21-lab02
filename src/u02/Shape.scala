package u02

object Shape {

  sealed trait Shape
  case class Rectangle(base: Double, height: Double) extends Shape
  case class Circle(radius: Double) extends Shape
  case class Square(side: Double) extends Shape

  def perimeter(shape: Shape): Double = shape match {
    case Rectangle(b, h) => b*2 + h*2
    case Square(s) => 4*s
    case Circle(r) => 2*r*Math.PI
  }

  def area(shape: Shape): Double = shape match {
    case Rectangle(b, h) => b*h
    case Circle(r) => Math.PI*r*r
    case Square(s) => s*s
  }

}
